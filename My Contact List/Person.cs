﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My_Contact_List
{
    class Person
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FavoriteWaifu { get; set; }

        public Person(int id, string name, string lastName, string favoriteWaifu)
        {
            this.ID = id.ToString();
            this.Name = name;
            this.LastName = lastName;
            this.FavoriteWaifu = favoriteWaifu;
        }
    }
}
