﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_Contact_List
{
    public partial class ContactList : Form
    {
        List<Person> PersonList = new List<Person>();        
        int IDCounter = 1;
        public ContactList()
        {
            InitializeComponent();
            // remove later

        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            Person person = new Person(IDCounter, txtName.Text, txtLastName.Text, txtFavoriteWaifu.Text);
            PersonList.Add(person);
            Clear();

            foreach (Person i in PersonList)
            {
                dgvList.Rows.Add(i.ID, i.Name + " " + i.LastName, i.FavoriteWaifu);

            }
            IDCounter++;


        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {

        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {

        }

        private void Clear()
        {
            txtName.Text = "";
            txtLastName.Text = "";
            txtFavoriteWaifu.Text = "";
            dgvList.Rows.Clear(); // added this to clear every row?
        }

        private void RefreshList()
        {
            foreach (Person person in PersonList)
            {
                dgvList.Rows.Add(new[] { person.ID, person.Name + " " + person.LastName, person.FavoriteWaifu }); ;
            }
            Clear();
        }

        private void DgvList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
